SUBROUTINE mm(Aw, Ak, Bw, Bk, Cw, Ck, A, B, C)
	INTEGER Aw, Ak, Bw, Bk, Cw, Ck, i, j, k
	REAL A(Aw, Ak), B(Bw, Bk), C(Cw, Ck)
	DO i = 1, Cw
		DO j = 1, Ck
			C(i, j) = 0
			DO k = 1, Ak
				C(i, j) = C(i, j) + A(i, k) * B(k, j)
			ENDDO
		ENDDO
	ENDDO
	RETURN
END

PROGRAM matrix_multiplication
	REAL A(12, 12), B(12, 12), C(12, 12)
	INTEGER Aw, Ak, Bw, Bk, Cw, Ck, i, j, k

	DO i = 1, 12
		DO j = 1, 12
			A(i, j) = 0
		ENDDO
	ENDDO

	DO i = 1, 12
		DO j = 1, 12
			B(i, j) = 0
		ENDDO
	ENDDO

	OPEN (1, file = '..\data.in')
	OPEN (2, file = '..\data.out')

	READ (1, *) Aw, Ak
	READ (1, *) Bw, Bk
	IF (Ak/=Bw) THEN
		PRINT *, 'Sizes mismatch'
		STOP
	ENDIF
	Cw = Aw
	Ck = Bk

	DO i = 1, Aw
		READ (1, *)(A(i, j), j = 1, Ak)
	ENDDO
	PRINT *, 'Matrix A'
	DO i = 1, Aw
		WRITE (*, *) (A(i, j), j = 1, Ak)
	ENDDO

	DO i = 1, Bw
		READ (1, *)(B(i, j), j = 1, Bk)
	ENDDO
	PRINT *, 'Matrix B'
	DO i = 1, Bw
		WRITE (*, *) (B(i, j), j = 1, Bk)
	ENDDO

	CALL mm(12, 12, 12, 12, 12, 12, A, B, C)

	DO i = 1, Cw
		WRITE (2, *)(C(i, j), j = 1, Ck)
	ENDDO
	STOP
END PROGRAM matrix_multiplication
